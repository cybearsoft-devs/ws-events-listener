"""This module contains script to be used to tun WS client."""

import asyncio
import logging

from websockets import connect

import src.common.settings as settings

from ws_client import WebSocketClient
from logging_config import SensitiveInfoFilter

logger = logging.getLogger()
logger.addFilter(SensitiveInfoFilter())
logging.basicConfig(format="%(asctime)s | %(levelname)s | %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S", level=settings.LOG_LEVEL)


async def repeat(interval: int, func: callable, *args, **kwargs):
    """
    Runs func every interval seconds.

    If func has not finished before *interval*, will run again
    immediately when the previous iteration finished.

    Parameters:
        interval (int): interval
        func (callable): function to be executed
        *args and **kwargs are passed as the arguments to func.
    Returns:
        None
    """
    while True:
        await asyncio.gather(
            func(*args, **kwargs),
            asyncio.sleep(interval),
        )


async def subscribe(client: WebSocketClient, graphql_query: str):
    """
    Subscribes to ws channel events.
    Parameters:
        client (WebSocketClient): WebSocketClient client
        graphql_query (str): Graphql query to be used for subscription
    Returns:
        None
    """
    await client.subscribe(channel=settings.WS_CHANNEL, api_key=settings.API_KEY)
    await asyncio.sleep(1)
    await client.query(graphql_query=graphql_query)


async def main():
    try:
        async with connect(settings.WS_URL) as ws:
            client       = WebSocketClient(ws)
            sub_interval = settings.WS_RESUB_INTERVAL

            for sub in settings.WS_SUBSCRIPTIONS:
                asyncio.ensure_future(repeat(sub_interval, subscribe, client, sub))

            while True:
                await client.process_message()
    except Exception as exc:
        logging.error(exc, exc_info=True)
        await asyncio.sleep(1)
        await main()


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
