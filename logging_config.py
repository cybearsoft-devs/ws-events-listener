"""This module contains logging config related logic."""

import re
import logging


class SensitiveInfoFilter(logging.Filter):
    """A class to mask sensitive log information."""
    API_KEY_PATTERN = r'''(?x)"APIKEY"\s* : \s*"((?:\\.|[^"])*)"'''

    def __init__(self, *patterns):
        """
        SensitiveInfoFilter initialization.
        Parameters:
            patterns (tuple): patterns that need to be used for
            sensitive information search.
        """
        super(SensitiveInfoFilter, self).__init__()
        self._patterns = [self.API_KEY_PATTERN, *patterns]

    def filter(self, record):
        record.msg = self._mask(record.msg)
        if isinstance(record.args, dict):
            for k in record.args.keys():
                record.args[k] = self._mask(record.args[k])
        else:
            record.args = tuple(self._mask(arg) for arg in record.args)
        return True

    def _mask(self, msg):
        """
        Masks sensitive information found via available patterns.
        Parameters:
            msg (str): original log message
        Returns:
            str
        """
        for pattern in self._patterns:
            for sensitive_data in re.findall(pattern, msg):
                msg = msg.replace(sensitive_data, "*****")
        return msg
