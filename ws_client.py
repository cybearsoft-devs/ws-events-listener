"""This module contains common WebSocket client related logic."""

import json
from logging import error, warning, info
from uuid import uuid5, NAMESPACE_X500

import jmespath as jmespath
from websockets.legacy.client import WebSocketClientProtocol

from src.common.utils import read_graphql_query

from src.domain.last_updates.tasks import update_cards
from src.domain.last_updates.tasks import update_token_offers_baseball
from src.domain.last_updates.tasks import update_token_offers
from src.domain.last_updates.tasks import update_cards_baseball


class EventDataProcessor(object):
    """
    A class to process WS server events data.
    """
    CARD_WAS_UPDATED_EVENT = "aCardWasUpdated"
    TOKEN_OFFER_WAS_UPDATED_EVENT = "tokenOfferWasUpdated"
    TOKEN_AUCTION_WAS_UPDATED_EVENT = "tokenAuctionWasUpdated"

    EVENT_TYPES = [
        CARD_WAS_UPDATED_EVENT,
        TOKEN_OFFER_WAS_UPDATED_EVENT,
        TOKEN_AUCTION_WAS_UPDATED_EVENT
    ]

    @staticmethod
    def _process_card_data(data: list):
        """
        Processes aCardWasUpdated event data and creates celery task.
        Parameters:
            data (dict): data to be processed
        Returns:
            None
        """
        update_cards.apply_async(kwargs=dict(nodes=data))

    @staticmethod
    def _process_token_auction_data(data: list):
        """
        Processes tokenAuctionWasUpdated event data and creates celery task.
        Parameters:
            data (dict): data to be processed
        Returns:
            None
        """
        auction = data[0]

        if not auction.get("open"):
            return

        prepped_nodes = auction["nfts"]
        update_cards_baseball.apply_async(kwargs=dict(nodes=prepped_nodes))

    @staticmethod
    def _process_token_offer_data(data: list):
        """
        Processes tokenOfferWasUpdated event data and creates celery task.
        Parameters:
            data (list): data to be processed
        Returns:
            None
        """
        token_offer  = data[0]
        blockchain_id = token_offer.get("blockchainId")

        if not blockchain_id and token_offer.get("acceptedAt"):
            prepped_nodes = jmespath.search("senderSide.nfts", token_offer)
            update_token_offers_baseball.apply_async(kwargs=dict(nodes=prepped_nodes))

        elif blockchain_id:
            update_token_offers.apply_async(kwargs=dict(nodes=data))

    def process(self, event_data: dict, event_type: str):
        """
        Processes event by its type.
        Parameters:
            event_data (dict): event data
            event_type (str): Event type to be processed
        Returns:
        """
        if event_type not in self.EVENT_TYPES:
            warning(f"'{event_type}' event type is not supported")
            return

        data = [event_data.get(event_type)]

        events_processes = {
            self.CARD_WAS_UPDATED_EVENT:
                lambda: self._process_card_data(data),
            self.TOKEN_OFFER_WAS_UPDATED_EVENT:
                lambda: self._process_token_offer_data(data),
            self.TOKEN_AUCTION_WAS_UPDATED_EVENT:
                lambda: self._process_token_auction_data(data),
        }

        return events_processes[event_type]()


class WebSocketClient(object):
    """
    A class to interact with WebSocket channel.
    """
    PING_MSG = "ping"
    DATA_MSG = "data"
    WELCOME_MSG = "welcome"
    CONFIRM_SUBSCRIPTION_MSG = "confirm_subscription"

    MSG_TYPES = [PING_MSG, DATA_MSG, WELCOME_MSG, CONFIRM_SUBSCRIPTION_MSG]

    def __init__(self, socket: WebSocketClientProtocol):
        """
        WebSocketClient initialization.
        Parameters:
            socket (WebSocketClientProtocol): WebSocket
        """
        self.ws = socket
        self.channel = None
        self.channel_id = None
        self.event_data_processor = EventDataProcessor()

    @staticmethod
    async def _random_id(channel: str):
        """
        Returns random id for particular channel.
        Parameters:
            channel (str): channel to subscribe to
        Returns:
            str
        """
        channel_id = str(uuid5(NAMESPACE_X500, f"{channel}-qwerty-api"))

        info(f"---> Generate '{channel_id}' id for '{channel}' channel.")
        return channel_id

    @staticmethod
    async def _extract_event_type(data: dict):
        """
        Extracts event type from data returned by WS server.
        Parameters:
            data (dict): data to extract event type from
        Returns:
            str
        """
        event     = None
        data_keys = list(data.keys())

        if data_keys:
            event = data_keys[0]

        return event

    async def _send(self, command: str, data: str = None, api_key: str = None):
        """
        Sends command to websockets channel.
        Parameters:
            command (str): command to be send to ws channel
            data (str): data to be send in command to ws channel
            api_key (str): API key
        Returns:
            None
        """
        identifier = {"channel": self.channel, "channelId": self.channel_id}
        command = {"command": command, "identifier": json.dumps(identifier)}

        if data:
            command["data"] = data
        if api_key:
            command["headers"] = dict(APIKEY=api_key)

        command = json.dumps(command)

        info(f"---> Send command to '{self.channel}' channel: {command}")
        await self.ws.send(command)

    async def _receive(self):
        """
        Receives data from ws channel.
        Parameters:
        Returns:
            dict
        """
        msg = await self.ws.recv()
        msg = json.loads(msg)

        return msg

    @staticmethod
    async def _process_welcome_message(msg: dict):
        """
        Processes welcome message.
        Parameters:
            msg (dict): message
        Returns:
            None
        """
        info(f"<--- Receive welcome message.")

    @staticmethod
    async def _process_ping_message(msg: dict):
        """
        Processes ping message.
        Parameters:
            msg (dict): message
        Returns:
            None
        """
        info(f"<--- Receive ping message {msg.get('message')}")

    @staticmethod
    async def _process_sub_confirm_message(msg: dict):
        """
        Processes subscription confirmed message.
        Parameters:
            msg (dict): message
        Returns:
            None
        """
        info(f"<--- Receive subscription confirmed message.")

    async def _process_data_message(self, msg: dict):
        """
        Processes data message.
        Parameters:
            msg (dict): message
        Returns:
            None
        """
        info(f"<--- Receive message from '{self.channel}' channel: {msg}")

        result = jmespath.search("message.result", msg)
        event  = await self._extract_event_type(result.get("data", {})) if result else None

        if event is None:
            return

        await self._process_event_data(result, event)

    async def process_message(self):
        """
        Processes messages by their types.
        Parameters:
        Returns:
            None
        """
        msg = await self._receive()
        msg_type = msg.get("type", self.DATA_MSG)

        if msg_type not in self.MSG_TYPES:
            warning(f"'{msg_type}' message type is not supported: {msg}")
            return

        msg_processes = {
            self.WELCOME_MSG:
                lambda: self._process_welcome_message(msg),
            self.PING_MSG:
                lambda: self._process_ping_message(msg),
            self.DATA_MSG:
                lambda: self._process_data_message(msg),
            self.CONFIRM_SUBSCRIPTION_MSG:
                lambda: self._process_sub_confirm_message(msg),
        }
        return await msg_processes[msg_type]()

    async def _process_event_data(self, result: dict, event: str):
        """
        Process data by event type.
        Parameters:
            result (dict): data to be insert or update in DB
            event (str): subscribed event
        Returns:
            str
        """
        task_id = None

        data   = result.get("data", {})
        errors = result.get("errors", {})

        if errors:
            error(f"Failed to create task for data update: {data}.")
            if isinstance(errors, list):
                error(errors[0].get('errors'))
            elif isinstance(errors, dict):
                error(errors.get('errors'))

        elif data and not errors:
            self.event_data_processor.process(data, event)

        return task_id

    async def send_message(self, data: dict):
        """
        Sends message command with data to ws channel.
        Parameters:
            data (dict): data to be send in command to ws channel
        Returns:
            None
        """
        await self._send(command="message", data=json.dumps(data))

    async def subscribe(self, channel: str, channel_id: str = None, api_key: str = None):
        """
        Sends subscribe command to ws channel.
        Parameters:
            channel (str): channel name to be subscribed to
            channel_id (str): channel unique id
            api_key (str): API key
        Returns:
            None
        """
        self.channel = channel
        self.channel_id = channel_id if channel_id else await self._random_id(channel)
        await self._send(command="subscribe", api_key=api_key)

    async def query(self, graphql_query: str, variables: dict = None,
                    operation_name: str = "offerUpdate", action: str = "execute"):
        """
        Sends graphql query to ws channel.
        Parameters:
            graphql_query (str): graphql query
            variables (dict): variables
            operation_name (str): operation name
            action (str): action type
        Returns:
            None
        """
        variables = variables if variables else {}

        query = read_graphql_query(graphql_query)
        data = {
            "query": query,
            "variables": variables,
            "operationName": operation_name,
            "action": action
        }
        await self.send_message(data=data)
